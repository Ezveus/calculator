package com.ezveus.calculator;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import expr.Expr;
import expr.Parser;

public class MainActivity extends Activity {
	private EditText output;
	private boolean dotIsNotLocked;
	private Editable input;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dotIsNotLocked = true;
        setAccordingToOrientation(getResources().getConfiguration().orientation);
    }

    private void setAccordingToOrientation(int orientation) {
    	if (output != null) {
    		input = output.getText();
    	}
    	if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
    		setContentView(R.layout.activity_main_horizontal);
	        output = (EditText)findViewById(R.id.output_horizontal);
    	} else {
	        setContentView(R.layout.activity_main_vertical);
	        output = (EditText)findViewById(R.id.output_vertical);
    	}
    	if (input != null) {
	        output.setText(input);
	    }
    }

  //   @Override
  //   public boolean onCreateOptionsMenu(Menu menu) {
  //   	getMenuInflater().inflate(R.menu.main, menu);
  //   	return true;
  //   }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onConfigurationChanged(newConfig);

   		setAccordingToOrientation(newConfig.orientation);
    }

    public void buttonClicked(View view) {
    	switch (view.getId()) {
			case R.id.bt_erase:
				output.setText("");
				dotIsNotLocked = true;
				break;
			case R.id.bt_backspace:
				try {
					if (output.getText().charAt(output.getText().length() - 1) == '.') {
						dotIsNotLocked = true;
					}
					removeLastCharacterFromEditable(output.getText());
				} catch (java.lang.IndexOutOfBoundsException e) {
				}
				break;
			case R.id.bt_1:
				output.append("1");
				break;
			case R.id.bt_2:
				output.append("2");
				break;
			case R.id.bt_3:
				output.append("3");
				break;
			case R.id.bt_4:
				output.append("4");
				break;
			case R.id.bt_5:
				output.append("5");
				break;
			case R.id.bt_6:
				output.append("6");
				break;
			case R.id.bt_7:
				output.append("7");
				break;
			case R.id.bt_8:
				output.append("8");
				break;
			case R.id.bt_9:
				output.append("9");
				break;
			case R.id.bt_0:
				output.append("0");
				break;
			case R.id.bt_add:
				try {
					dotIsNotLocked = true;
					switch (output.getText().charAt(output.getText().length() - 1)) {
						case '-':
						case '*':
						case '/':
							output.setText(removeLastCharacterFromEditable(output.getText()) + "+");
						case '+':
							break;
						default:
							output.append("+");
							break;
					}
				} catch (java.lang.IndexOutOfBoundsException e) {
				}
				break;
			case R.id.bt_substract:
				try {
					dotIsNotLocked = true;
					switch (output.getText().charAt(output.getText().length() - 1)) {
						case '+':
							output.setText(removeLastCharacterFromEditable(output.getText()) + "-");
							break;
						case '*':
						case '/':
							output.append("-");
							break;
						case '-':
							break;
						default:
							output.append("-");
							break;
					}
				} catch (java.lang.IndexOutOfBoundsException e) {
					output.append("-");
				}
				break;
			case R.id.bt_multiply:
			try {
					dotIsNotLocked = true;
					switch (output.getText().charAt(output.getText().length() - 1)) {
						case '+':
						case '-':
						case '/':
							output.setText(removeLastCharacterFromEditable(output.getText()) + "*");
						case '*':
							break;
						default:
							output.append("*");
							break;
					}
				} catch (java.lang.IndexOutOfBoundsException e) {
				}
				break;
			case R.id.bt_divide:
				try {
					dotIsNotLocked = true;
					switch (output.getText().charAt(output.getText().length() - 1)) {
						case '+':
						case '-':
						case '*':
							output.setText(removeLastCharacterFromEditable(output.getText()) + "/");
						case '/':
							break;
						default:
							output.append("/");
							break;
					}
				} catch (java.lang.IndexOutOfBoundsException e) {
				}
				break;
			case R.id.bt_equal:
				if (output.getText() != null && output.getText().length() > 0) {
					output.setText(calculate(output.getText().toString()));
				}
				dotIsNotLocked = true;
				break;
			case R.id.bt_dot:
				if (dotIsNotLocked) {
					output.append(".");
					dotIsNotLocked = false;
				}
				break;
			default:
				break;
    	}
    }

    public void functionClicked(View view) {
    	switch (view.getId()) {
			case R.id.bt_sin:
				dotIsNotLocked = true;
				output.append("sin(");
				break;
			case R.id.bt_cos:
				dotIsNotLocked = true;
				output.append("cos(");
				break;
			case R.id.bt_tan:
				dotIsNotLocked = true;
				output.append("tan(");
				break;
			case R.id.bt_exp:
				dotIsNotLocked = true;
				output.append("exp(");
				break;
			case R.id.bt_log:
				dotIsNotLocked = true;
				output.append("log(");
				break;
			case R.id.bt_rnd:
				dotIsNotLocked = true;
				output.append("round(");
				break;
			case R.id.bt_sqrt:
				dotIsNotLocked = true;
				output.append("sqrt(");
				break;
			case R.id.bt_hat:
				dotIsNotLocked = true;
				output.append("^");
				break;
			case R.id.bt_obr:
				dotIsNotLocked = true;
				output.append("(");
				break;
			case R.id.bt_cbr:
				dotIsNotLocked = true;
				output.append(")");
				break;
			case R.id.bt_pi:
				dotIsNotLocked = true;
				output.append("pi");
				break;
			default:
				break;
    	}
    }

    private Editable removeLastCharacterFromEditable(Editable s) {
    	if (s.length() > 0) {
    		s.delete(s.length() - 1, s.length());
    	}
    	return s;
    }

    protected String calculate(String expression) {
    	Expr e;
    	try	{
 			e = Parser.parse(expression);
    	} catch (expr.SyntaxException except) {
    		return getString(R.string.error);
    	}
    	return Double.toString(e.value());
    }
}
