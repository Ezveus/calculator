# Calculator

A simple Android calculator for the first "Mobile Development" assignment.

## Build (easily) and deploy

	$ android update project -p .	# Create the local.properties with the path to the SDK
	$ ant debug				# Build a debug version of the project
	# If you only have one device wired to your computer
	$ ant installd			# Install on the default device
	# else if two or more devices are wired
	$ adb -s <DEVICE_ID> install ./bin/calculator-debug.apk

Then you just have to launch the calculator on your device.

## Caveat

All code under src/expr belongs to Darius Bacon's ['expr' project](https://github.com/darius/expr).
See COPYING_expr.txt for more informations about this code license.
